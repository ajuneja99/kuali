class Elevator {
    constructor(numFloors) {
        this.policy = policy
        this.currFloor = 0
        this.numFloors = numFloors
        this.pickUpRequests = []   //Associative array mapping floorNumber to a list of pending requests
        this.dropOffRequests = []
        this.stopped = true
        this.direction = "UP"   //TODO:Replace with ENUM
        this.inService = true
        this.numTrips = 0
        this.nextStop = 1
    }

    start() {
        this.running = true
        //TODO: Start in new thread
        while(this.running) {
            this.checkDirection()
            this.move()
            if(this.currFloor in this.pickUpRequests || this.currFloor in this.dropOffRequests) { //chg
                this.stopAtFloor()
            } 
        }
    }

    stop() {
        this.running = false
    }

    move() {
        this.nextStop = this.getNextStop()
        this.checkDirection()
        while(this.currFloor != this.nextStop) {    //Simulate the movement of an elevator
            if(this.direction=="DOWN") {
                this.currFloor = this.currFloor-1;
            }
            else if(this.direction=="UP") {
                this.currFloor = this.currFloor+1;
            }
        }

    }
    checkDirection() {
        if( (this.currFloor > this.nextStop) || (this.currFloor == this.numFloors && this.direction === "UP")) {
            this.direction = "DOWN"
        }
        else if( (this.currFloor < this.nextStop) || (this.currFloor == 1 && this.numFloors === "DOWN")) {
            this.direction = "UP"
        }
    }
    stopAtFloor() {
        /**
         * When the elevator stops, pick up people at the current floor and/or drop of people at the current floor
         * and empty both arrays
         */
        listPeopleToDropOff = this.dropOffRequests[this.currFloor]
        this.capacity -= listPeopleToDropOff.length
        this.dropOffRequests[this.currFloor] = []
        
        var pickUpRequestsCurrentFloor = this.pickUpRequests[this.currFloor]
        for(var i = 0; i < pickUpRequestsCurrentFloor.length; i++) {
            //Add people from the pickup request at the current floor to the drop off list
            var newRequest = pickUpRequestsCurrentFloor[i]
            this.dropOffRequests[newRequest.to].push(newRequest)
            this.capacity += newRequest.numPeople
        }

        //Empty pickup requests for the current floor once they have been served
        this.pickUpRequests[this.currFloor] = []
    }

        getNextStop() {
            //TODO: Write extensive unit tests for this method, lots of potential edge cases
            var nextStop = null;
            if(this.pickUpRequests.length == 0 && this.dropOffRequests.length == 0)
                nextStop = this.nextFloor()
            else if(this.pickUpRequests.length == 0)
                nextStop = this.dropOffRequests[0]
            else if(this.dropOffRequests[0].length ==0)
                nextStop = this.pickUpRequests[0]
            else { 
                //Find the closest pickup and dropoff locations from the current floor and take the smaller number as the next stop
                var indexOfNearestPickup = this.pickUpRequests.indexOf(Math.min(this.pickUpRequests).map((req) => math.abs(req.from-this.currFloor)))
                var indexOfNearestDropoff = this.dropOffRequests.indexOf(Math.min(this.dropOffRequests.map((req) => math.abs(req.to-this.currFloor))))
                nextStop = Math.min(this.pickUpRequests[indexOfNearestPickup],this.dropOffRequests[indexOfNearestDropoff])
            }
            return nextStop
    }

    willPassThrough(floor) {
        /*If the input floor is in the range between current floor and the next stop it will pass through*/ 
        if(this.currFloor > this.nextStop && this.nextStop < floor && this.direction == "DOWN" ) {
          return true  
        }
        else if(this.currFloor < this.nextStop && this.nextStop > floor && this.direction == "UP" ) {
            return true
        }
        else {
            return false
        }

    }

    isInService() {
        return this.inService
    }
}