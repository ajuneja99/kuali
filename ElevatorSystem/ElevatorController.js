class ElevatorController {
    constructor(elevators,policy) {
        this.running = false
        this.elevators = elevators
        this.policy = policy
        this.requestQueue = []
        //TODO: these constants would be read from a config file as the throughput might vary based on the cpu/memory of the specific machine
        this.K = 5
        this.C = 2
    }

    processRequest() {
        //Run on a seperate thread
        while(this.running) {
            if(this.requestQueue.length != 0) {
                request = requestQueue[0]
                this.stopElevators()   //freeze elevators current state while policy is making a decision
                this.policy.processRequest(request,this.elevators)
                this.requestQueue.pop(0) //remove request only after process is completed succsessfully
                                         //in full implementation add retry logic, and add deadletter queue for failed requests/further analysis
                this.startElevators()
            }
            sleep(this.getSleepTime())
        }
    }
    
    getSleepTime() {
        return 1/(1 + this.requestqueue.length) * this.K + this.C
   }
}