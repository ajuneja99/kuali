var Elevator = require("./Elevator")
var ElevatorSystem = require("./ElevatorController")
class ElevatorFactory {
    /*Responsible for composing all the objects for creating an ElevatorSystem */

    createElevatorSystem(numElevators,numFloors,policy) {
        //TODO: Create Elevators, Controllers, ElevatorSystem with given policy
    }

    
    createElevators(n,policy,numFloors){
        //TODO: Return list of n elevator objects
    }

    createElevatorControllers() {
        //TODO: Create Controllers
    }

}

module.exports = ElevatorFactory;