class ElevatorSystem {
    constructor(controllers) {
        this.controllers = controllers
        this.faultyControllers = [] //addresses of controllers that return err or timeout so a technican can be alerted
        this.active = 0
    }

    newRequest(request) {
        if(this.controllers.length == 0)
            throw new Exception("All controllers down - please use stairs") //TODO: declare in constants file
        try {
            this.controllers[this.active].processRequest(request)
            this.active = (this.active+1)%this.controllers.length //Round robin scheduling of all controllers to evenly distribute load
        } 
        catch(err) { 
            console.log(err) //in real implementation route error to logging system etc: graylog, splunk, logstash etc.
            this.faultyControllers.push(this.controllers[this.active])
            this.controllers.pop(this.active) //remove the active controller from the list of controllers so we don't use it again
            this.active = (this.active+1)%this.controllers.length
            this.controllers[this.active].startController() = true
            return this.newRequest(request) //recursively retry till no more active controllers
        } 
        return "Request processed succsessfully" //TODO: declare error code/message in constants file
    }
}