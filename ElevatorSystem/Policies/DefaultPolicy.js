class DefaultPolicy {

    handleRequest(elevators,request) {
            var elevator = this.selectElevatorToProcessRequest(elevator,request)
            elevator.addRequest(request)
    }

    selectElevatorToProcessRequest() {
        var destination = request.from;
        //to avoid sort operation on each request an alternative implementation may use a sorted array or some sort of PQ data structure
        //should be roughly equal since number of elevators will usually be a factor of 10 not hundreds of thousands's or millions so nlog(n) [sort time] ~ n [time to insert/shift elements in sorted list]
        var sortedElevators = elevators.sort(elevator => math.abs(elevator.currFloor - destination)) 
        var occupiedElevatorsAtCurrentFloor = sortedElevators.filter(elevator => elevator.currFloor == destination && !elevator.isOccupied)
        
        if(occupiedElevatorsAtCurrentFloor.length != 0) {
            return occupiedElevatorsAtCurrentFloor[0]
        }

        var occupiedMovingInDirection = sortedElevators.filter(elevator => elevator.willPassThrough(destination) && !elevator.isOccupied())
        if(occupiedMovingInDirection.length != 0) {
            //the elevators are already sorted by distance to the current floor
            //so this will return the closest occupied elevator moving in the same direction
           return occupiedMovingInDirection[0]
        }

        else {
            return sortedElevators[0]
        }

    }
}
